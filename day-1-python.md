# Python, eine Einführung

## Installation/Distribution
* Miniconda, Conda.
* Pip

## Geschichte
* Beginn ~1990 Guido van Rossum
* Python 2: ~2000
* Python 3: ~2008

## Python, was ist das?

* höhere Programmiersprache
* multiparadigmatisch: imperativ, objektorientiert, funktional
* zumeist interpretiert
* mehrere Implementierungen (CPython, PyPy, Jython, IronPython, ...)
* dynamisch typisiert / "stark" / "Duck Typing"
* Häufig als Scriptsprache benutzt
* Einrückung ist wichtig

## Python: Elemente

* dynamisch typisiert: Nutzung entscheidet den Typen
* Elementare Datentypen: z.B. int, float, complex,
* Aggregate: strings, Listen, Tupel, Dictionaries, Sets, Objekte
* "Prinzipiell" alles Objekte

## Operationen auf den Datentypen

* Slices
* Gleichheit
* Comprehensions

## Kontrollstrukturen I:

* if .. elif .. else
* for
* while
* try .. except
* with

## Funktionen

* Default-Argumente
* Keyword-Argmuente
* Variable Length-Argumente
* Innere Funktionen
* Lambdas
* Sichtbarkeiten
* Rekursion

## Kontrollstrukturen II

* Iteratoren
* Generatoren
* Dekoratoren

## Klassen

* Methoden
* Klassen-Methoden
* Vererbung
* Meta-Klassen

## Module und Packages

* Python-Module
* C-Module
* PYTHONPATH
* imports

## Dictionaries in der Tiefe

* Attribute und Properties

## Standard-Libraries: Batteries included.

