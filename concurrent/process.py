#!/usr/bin/env python3

from multiprocessing import Pool

def fac(n):
    f = 1
    while n >= 1:
        f *= n
        n -= 1
    return f

if __name__ == '__main__':

    print(fac(0)+fac(1)+fac(2)+fac(3))
    print(sum(map(fac, range(4))))

    p = Pool(4)
    print(sum(p.map(fac, range(5000))))
