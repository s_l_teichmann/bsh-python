#!/usr/bin/env python3

from threading import Thread
from random import randint
from time import sleep

class MyThread(Thread):

    def __init__(self, no):
        super().__init__()
        self.no = no

    def run(self):
        for i in range(5):
            print(self.no)
            sleep(randint(1, 4))

def main():
    t1 = MyThread(1)
    t2 = MyThread(2)
    t1.start()
    t2.start()
    t1.join()
    t2.join()


if __name__ == '__main__':
    main()
