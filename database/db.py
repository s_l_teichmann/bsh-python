#!/usr/bin/env python

import sqlite3 as db

dbName = "demo.db"

schema = """
CREATE TABLE IF NOT EXISTS entries (
    id     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name   VARCHAR NOT NULL,
    points INTEGER NOT NULL DEFAULT 0
)
"""

insertStmt = "INSERT INTO entries (name, points) VALUES (?, ?)"

selectStmt = "SELECT id FROM entries WHERE name=?"
deleteStmt = "DELETE FROM entries WHERE id=?"

data = [
    ('Klaus', 43),
    ('Petra', 121),
]


def createTable(conn):
    c = conn.cursor()
    try:
        c.execute(schema)
        conn.commit()
    finally:
        c.close()

def insertData(conn, data):
    c = conn.cursor()
    try:
        c.executemany(insertStmt, data)
        conn.commit()
    finally:
        c.close()

def selectData(conn, name):
    c = conn.cursor()
    try:
        c.execute(selectStmt, (name,))
        return [r[0] for r in c.fetchall()]
    finally:
        c.close()

def deleteData(conn, ids):
    c = conn.cursor()
    try:
        for id in ids:
            c.execute(deleteStmt, (id,))
        conn.commit()
    finally:
        c.close()
    conn.commit()


def main():
    with db.connect(dbName) as conn:
        createTable(conn)
        insertData(conn, data)
        ids = selectData(conn, 'Klaus')
        print(ids)
        deleteData(conn, ids)


if __name__ == '__main__':
    main()
