#!/usr/bin/env python

from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Entry(Base):
    __tablename__ = 'entries'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(250), nullable=False)
    points = Column(String(250), nullable=False)


def main():
    engine = create_engine('sqlite:///alchemy.db')
    Base.metadata.create_all(engine)
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    klaus = Entry(name='Klaus', points=43)
    petra = Entry(name='Petra', points=121)
    session.add(klaus)
    session.add(petra)
    session.commit()

    klauses = session.query(Entry).filter(Entry.name == 'Klaus').all()
    print([k.id for k in klauses])
    for k in klauses:
        session.delete(k)
    session.commit()
    session.close()

if __name__ == "__main__":
    main()
