# Schulung BSH: Python, Scientific Python und Geo-Python

## Agenda

* Tag 1: [Python, eine Einführung](day-1-python.md)
* Tag 2: [Scientific Python](day-2-scientific.md)
* Tag 3: [Geo-Python](day-3-geo.md)
