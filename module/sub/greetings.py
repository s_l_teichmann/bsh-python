def greet(msg = 'Hello', name='John Doe'):
    print("{}, {}".format(msg, name))

if __name__ == '__main__':
    greet()
