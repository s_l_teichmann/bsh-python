# Scientific Python

* [Jupyter (IPython)](http://jupyter.org/)
* [NumPy](http://www.numpy.org/)
* [SciPy](https://www.scipy.org/)
* [Matplotlib](http://matplotlib.org/)
* [pandas](http://pandas.pydata.org/)
