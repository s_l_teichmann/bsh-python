# Geo-Python

[Extern](https://github.com/BjoernSchilberg/geopython)

* Allgemein
* Meereisdaten
* Rasterdaten
* Shapefiles
* GPS (Python 2)
* Projektionen
* NetCDF
